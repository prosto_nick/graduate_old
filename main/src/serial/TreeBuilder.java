package serial;

import javax.swing.*;
import java.util.*;
import java.util.stream.Collectors;

import static java.sql.Types.INTEGER;
import static java.sql.Types.NULL;

public class TreeBuilder {

    final int left=0;
    final int right=1;

    private int countAllRows (List<List<List<Float>>> groups) {
        int allRows = 0;
        for (List<List<Float>> group : groups) {
            //if (group.size() == NULL) continue;
            allRows += group.size();
        }
        return allRows;
    }

    private int countClassInGroup (float classNumber, List<List<Float>> group){
        int quantity = 0;
        for (List<Float> row : group) {
            if (row.get(row.size()-1).equals(classNumber)){
                quantity++;
            }
        }
        return quantity;
    }

    float getGinni (List<Float> classes, List<List<List<Float>>> groups){

        float ginni=0;

        for (List<List<Float>> group : groups) {

            if(group.isEmpty())  continue;

            float score = 0;

            for (Float classNumb : classes) {

                float proportion = (float) countClassInGroup(classNumb, group) / group.size();
                score += proportion*proportion;

            }
            ginni += (1 - score) * group.size() / countAllRows(groups);

        }
        return ginni;
    }

    List<List<List<Float>>> splitOnGroups (int column, float columnValue, List<List<Float>> dataset) {

        List<List<Float>> leftGroup = new ArrayList<>();
        List<List<Float>> rightGroup = new ArrayList<>();

        for (List<Float> row : dataset) {
            if (row.get(column) < columnValue){
                leftGroup.add(row);
            } else {
                rightGroup.add(row);
            }
        }

        return Arrays.asList(leftGroup, rightGroup);

    }

    Map<Class, Object> bestSplit (List<List<Float>> dataset){

        int bestColumn = Integer.MAX_VALUE;
        float bestValue = Float.MAX_VALUE;
        float bestGinni = Float.MAX_VALUE;

        List<List<List<Float>>> bestGroups = new ArrayList<>();

        Map<Class, Object> result = new HashMap<>();

        for (List<Float> row : dataset) {
            for (int column=0; column<row.size()-1; column++) {
                List<List<List<Float>>>  groups = splitOnGroups(column, row.get(column), dataset);
                if (getGinni(countClasses(dataset), groups) < bestGinni){

                    bestColumn = column;
                    bestValue = row.get(column);
                    bestGinni = getGinni(countClasses(dataset), groups);
                    bestGroups = groups;
                }
            }
        }
        result.put(Integer.class, bestColumn);
        result.put(Float.class, bestValue);
        result.put(List.class, bestGroups);
        return result;
    }

    private List<Float> countClasses (List<List<Float>> dataset){
        return dataset.stream()
                .map(e -> e.get(e.size() - 1))
                .distinct()
                .collect(Collectors.toList());
//        int quantity = 0;
//        for (List<Float> row : group) {
//            if (row.get(row.size()-1).equals(classNumber)){
//                quantity++;
//            }
//        }
//        return quantity;
    }

    private float countClassesInGroup (List<List<Float>> group) {

        List<Float> classes = countClasses(group);
        int maxClass=0;
        float bestClassForUs=0;
        for (Float classNumb : classes) {

            int crtClass = countClassInGroup(classNumb,group);
            if (crtClass > maxClass) {bestClassForUs = classNumb;}
        }
        return bestClassForUs;
    }

    void split (Node node, int maxDepth, int minSizeOfGroup, int crtDepth, List<List<List<Float>>> groups) {

        Map <Class, Object> crt;

        if (groups.get(left).isEmpty() || groups.get(right).isEmpty()) {
            if (!groups.get(left).isEmpty()) {
                node.setLeftLeaf(countClassesInGroup(groups.get(left)));
            } else {
                node.setRightLeaf(countClassesInGroup(groups.get(right)));
            }
            return;
        }

        if (crtDepth >= maxDepth) {
            node.setLeftLeaf(countClassesInGroup(groups.get(left)));
            node.setRightLeaf(countClassesInGroup(groups.get(right)));
            return;
        }

        if (groups.get(left).size() <= minSizeOfGroup){
            node.setLeftLeaf(countClassesInGroup(groups.get(left)));

        } else {
            crt = bestSplit(groups.get(left));
            node.setLeftChild(new Node((int) crt.get(Integer.class), (float) crt.get(Float.class)));
            split (node.getLeftChild(), maxDepth, minSizeOfGroup, crtDepth+1,
                    (List<List<List<Float>>>) crt.get(List.class));
        }

        if (groups.get(right).size() <= minSizeOfGroup){
            node.setRightLeaf(countClassesInGroup(groups.get(right)));

        } else {
            crt = bestSplit(groups.get(right));
            node.setRightChild(new Node((int) crt.get(Integer.class), (float) crt.get(Float.class)));
            split (node.getRightChild(), maxDepth, minSizeOfGroup, crtDepth+1,
                    (List<List<List<Float>>>) crt.get(List.class));
        }
    }

    Node buildTree (List<List<Float>> train, int maxDepth, int minSizeOfGroup) {

        Map <Class, Object> crt = bestSplit(train);
        Node root = new Node((int) crt.get(Integer.class), (float) crt.get(Float.class));
        split(root, maxDepth, minSizeOfGroup, 1, (List<List<List<Float>>>) crt.get(List.class));
        return root;
    }

    void printTree (Node node, String shift) {

        //Проверяем узел на ноль, если ноль - выводим *, иначе - выводим себя.
        if (node != null){
            System.out.println(shift + node);
            printTree(node.getLeftChild(), shift + "  ");
            printTree(node.getRightChild(), shift + "  ");

        }


    }

}
