package functional;

import functional.Group;
import functional.TreeBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class main {

    public static void main(String[] args) {


            Group dataset = new Group(Arrays.asList(
                    Arrays.asList(2.771244718f, 1.784783929f, 0f),
                    Arrays.asList(1.728571309f, 1.169761413f, 0f),
                    Arrays.asList(3.678319846f, 2.81281357f, 0f),
                    Arrays.asList(3.961043357f, 2.61995032f, 0f),
                    Arrays.asList(2.999208922f, 2.209014212f, 0f),
                    Arrays.asList(7.497545867f, 3.162953546f, 1f),
                    Arrays.asList(9.00220326f, 3.339047188f, 1f),
                    Arrays.asList(7.444542326f, 0.476683375f, 1f),
                    Arrays.asList(10.12493903f, 3.234550982f, 1f),
                    Arrays.asList(6.642287351f, 3.319983761f, 1f)));


        TreeBuilder builder = new TreeBuilder();

            Map<String, Object> root = builder.buildTree(dataset, 1, 1);

            builder.printTree(root, "");

    }
}
