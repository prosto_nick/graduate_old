package serial;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class TreeBuilderTest {
    @Test
    void Test1() {
        List<Float> classes = Arrays.asList(0f, 1f);

        List<List<Float>> firstGroup = new ArrayList<>();
        firstGroup.add(Arrays.asList(1f, 1f));
        firstGroup.add(Arrays.asList(2f, 1f));

        List<List<Float>> secondGroup = new ArrayList<>();
        secondGroup.add(Arrays.asList(0f, 0f));
        secondGroup.add(Arrays.asList(1f, 0f));

        List<List<List<Float>>> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0f, builder.getGinni(classes, groups));

    }

    @Test
    void Test2() {
        List<Float> classes = Arrays.asList(0f, 1f);

        List<List<Float>> firstGroup = new ArrayList<>();
        firstGroup.add(Arrays.asList(1f, 0.3f, 1f, 0f));
        firstGroup.add(Arrays.asList(2f, 0f, 1f, 0f));
        firstGroup.add(Arrays.asList(3f, 0f, 1f, 1f));
        firstGroup.add(Arrays.asList(2f, 0f, 1f, 1f));

        List<List<Float>> secondGroup = new ArrayList<>();
        secondGroup.add(Arrays.asList(5f, 0.1f, 0f, 1f));
        secondGroup.add(Arrays.asList(1f, 0f, 2f, 0f));
        secondGroup.add(Arrays.asList(2f, 0.5f, 1f, 1f));
        secondGroup.add(Arrays.asList(3f, 0.2f, 1f, 0f));

        List<List<List<Float>>> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0.5f, builder.getGinni(classes, groups));
    }

    @Test
    void Test3(){
        List<Float> classes = Arrays.asList(0f, 1f);

        List<List<Float>> firstGroup = new ArrayList<>();

        List<List<Float>> secondGroup = new ArrayList<>();
        secondGroup.add(Arrays.asList(0f, 0f));
        secondGroup.add(Arrays.asList(0f, 1f));
        secondGroup.add(Arrays.asList(1f, 0f));
        secondGroup.add(Arrays.asList(2f, 0f));

        List<List<List<Float>>> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0.375f, builder.getGinni(classes, groups));
    }

    @Test
    void Test4(){
        List<Float> classes = Arrays.asList(0f, 1f);

        List<List<Float>> firstGroup = new ArrayList<>();
        firstGroup.add(Arrays.asList(5f, 2f, 1f));
        firstGroup.add(Arrays.asList(0f, 0f, 1f));
        firstGroup.add(Arrays.asList(1f, 1f, 1f));
        firstGroup.add(Arrays.asList(0f, 1f, 1f));
        firstGroup.add(Arrays.asList(1f, 0f, 1f));
        firstGroup.add(Arrays.asList(0f, 0f, 1f));
        firstGroup.add(Arrays.asList(2f, 0f, 0f));
        firstGroup.add(Arrays.asList(2f, 1f, 0f));

        List<List<Float>> secondGroup = new ArrayList<>();
        secondGroup.add(Arrays.asList(0f, 0f, 0f));
        secondGroup.add(Arrays.asList(0f, 1f, 1f));
        secondGroup.add(Arrays.asList(1f, 0f, 0f));
        secondGroup.add(Arrays.asList(1f, 1f, 1f));

        List<List<List<Float>>> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0.4166667f, builder.getGinni(classes, groups));
    }

    @Test
    void Test5(){
        List<Float> classes = Arrays.asList(0f, 1f);

        List<List<Float>> firstGroup = new ArrayList<>();
        firstGroup.add(Arrays.asList(0.5f, 1.2f, 0f));
        firstGroup.add(Arrays.asList(0.3f, 0.8f, 0f));
        firstGroup.add(Arrays.asList(0.7f, 1.1f, 1f));


        List<List<Float>> secondGroup = new ArrayList<>();
        secondGroup.add(Arrays.asList(1.0f, 0.5f, 0f));
        secondGroup.add(Arrays.asList(0.9f, 1.3f, 1f));


        List<List<List<Float>>> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0.46666664f, builder.getGinni(classes, groups));

    }

    @Test
    void Test6() {
        List<Float> classes = Arrays.asList(0f, 1f);

        List<List<Float>> firstGroup = new ArrayList<>();
        firstGroup.add(Arrays.asList(0.5f, 1.2f, 0f));
        firstGroup.add(Arrays.asList(0.3f, 0.8f, 0f));

        List<List<Float>> secondGroup = new ArrayList<>();
        secondGroup.add(Arrays.asList(0.7f, 1.1f, 1f));
        secondGroup.add(Arrays.asList(1.0f, 0.5f, 0f));
        secondGroup.add(Arrays.asList(0.9f, 1.3f, 1f));


        List<List<List<Float>>> groups = Arrays.asList(firstGroup, secondGroup);

        TreeBuilder builder = new TreeBuilder();

        assertEquals(0.26666665f, builder.getGinni(classes, groups));
    }

    @Test
    void TestSplit1(){

        List<List<Float>> dataset = new ArrayList<>();
        dataset.add(Arrays.asList(1.1f, 2.4f, 0f));
        dataset.add(Arrays.asList(3.5f, 2.2f, 1f));
        dataset.add(Arrays.asList(1.6f, 2.8f, 0f));
        dataset.add(Arrays.asList(0.9f, 3.5f, 0f));
        dataset.add(Arrays.asList(1.8f, 1.5f, 0f));
        dataset.add(Arrays.asList(4.1f, 3.3f, 1f));
        dataset.add(Arrays.asList(3.7f, 2.9f, 1f));
        dataset.add(Arrays.asList(1.9f, 2.7f, 0f));

        List<List<Float>> firstGroup = new ArrayList<>();
        firstGroup.add(Arrays.asList(1.1f, 2.4f, 0f));
        firstGroup.add(Arrays.asList(1.6f, 2.8f, 0f));
        firstGroup.add(Arrays.asList(0.9f, 3.5f, 0f));
        firstGroup.add(Arrays.asList(1.8f, 1.5f, 0f));
        firstGroup.add(Arrays.asList(1.9f, 2.7f, 0f));

        List<List<Float>> secondGroup = new ArrayList<>();
        secondGroup.add(Arrays.asList(3.5f, 2.2f, 1f));
        secondGroup.add(Arrays.asList(4.1f, 3.3f, 1f));
        secondGroup.add(Arrays.asList(3.7f, 2.9f, 1f));

        List<List<List<Float>>> groups = Arrays.asList(firstGroup, secondGroup);

        int column = 0;
        float value = 3.5f
                ;
        TreeBuilder builder = new TreeBuilder();

        assertEquals(groups,  builder.splitOnGroups(column, value, dataset));

    }

    @Test
    void TestOnBestSplit1 (){
        List<List<Float>> dataset = new ArrayList<>();
        dataset.add(Arrays.asList(1.1f, 2.4f, 0f));
        dataset.add(Arrays.asList(3.5f, 2.2f, 1f));
        dataset.add(Arrays.asList(1.6f, 2.8f, 0f));
        dataset.add(Arrays.asList(0.9f, 3.5f, 0f));
        dataset.add(Arrays.asList(1.8f, 1.5f, 0f));
        dataset.add(Arrays.asList(4.1f, 3.3f, 1f));
        dataset.add(Arrays.asList(3.7f, 2.9f, 1f));
        dataset.add(Arrays.asList(1.9f, 2.7f, 0f));

        List<List<Float>> firstGroup = new ArrayList<>();
        firstGroup.add(Arrays.asList(1.1f, 2.4f, 0f));
        firstGroup.add(Arrays.asList(1.6f, 2.8f, 0f));
        firstGroup.add(Arrays.asList(0.9f, 3.5f, 0f));
        firstGroup.add(Arrays.asList(1.8f, 1.5f, 0f));
        firstGroup.add(Arrays.asList(1.9f, 2.7f, 0f));

        List<List<Float>> secondGroup = new ArrayList<>();
        secondGroup.add(Arrays.asList(3.5f, 2.2f, 1f));
        secondGroup.add(Arrays.asList(4.1f, 3.3f, 1f));
        secondGroup.add(Arrays.asList(3.7f, 2.9f, 1f));

        List<List<List<Float>>> groups = Arrays.asList(firstGroup, secondGroup);
        int bestColumn = 0;
        float bestValue = 3.5f;

        Map<Class, Object> result = new HashMap<>();
        result.put(Integer.class, bestColumn);
        result.put(Float.class, bestValue);
        result.put(List.class, groups);
        TreeBuilder builder = new TreeBuilder();

        assertEquals(result,  builder.bestSplit(dataset));
    }

    @Test
    void TestOnBestSplit2 (){
        List<List<Float>> dataset = new ArrayList<>();
        dataset.add(Arrays.asList(0.5f, 1.2f, 0f));
        dataset.add(Arrays.asList(0.3f, 0.8f, 0f));
        dataset.add(Arrays.asList(0.7f, 1.1f, 1f));
        dataset.add(Arrays.asList(1.0f, 0.5f, 0f));
        dataset.add(Arrays.asList(0.9f, 1.3f, 1f));


        List<List<Float>> firstGroup = new ArrayList<>();
        firstGroup.add(Arrays.asList(0.5f, 1.2f, 0f));
        firstGroup.add(Arrays.asList(0.3f, 0.8f, 0f));

        List<List<Float>> secondGroup = new ArrayList<>();
        secondGroup.add(Arrays.asList(0.7f, 1.1f, 1f));
        secondGroup.add(Arrays.asList(1.0f, 0.5f, 0f));
        secondGroup.add(Arrays.asList(0.9f, 1.3f, 1f));

        List<List<List<Float>>> groups = Arrays.asList(firstGroup, secondGroup);
        int bestColumn = 0;
        float bestValue = 0.5f;

        Map<Class, Object> result = new HashMap<>();
        result.put(Integer.class, bestColumn);
        result.put(Float.class, bestValue);
        result.put(List.class, groups);
        TreeBuilder builder = new TreeBuilder();

        assertEquals(result,  builder.bestSplit(dataset));
    }

    @Test
    void TestOnBestSplit3() {
        List<List<Float>> dataset = new ArrayList<>();
        dataset.add(Arrays.asList(1f, 4f, 1f));
        dataset.add(Arrays.asList(3f, 7f, 0f));
        dataset.add(Arrays.asList(2f, 1f, 1f));
        dataset.add(Arrays.asList(3f, 2f, 0f));
        dataset.add(Arrays.asList(2f, 3f, 0f));

        List<List<Float>> firstGroup = new ArrayList<>();
        firstGroup.add(Arrays.asList(1f, 4f, 1f));
        firstGroup.add(Arrays.asList(2f, 1f, 1f));
        firstGroup.add(Arrays.asList(2f, 3f, 0f));

        List<List<Float>> secondGroup = new ArrayList<>();
        secondGroup.add(Arrays.asList(3f, 7f, 0f));
        secondGroup.add(Arrays.asList(3f, 2f, 0f));


        List<List<List<Float>>> groups = Arrays.asList(firstGroup, secondGroup);
        int bestColumn = 0;
        float bestValue = 2f;

        Map<Class, Object> result = new HashMap<>();
        result.put(Integer.class, bestColumn);
        result.put(Float.class, bestValue);
        result.put(List.class, groups);
        TreeBuilder builder = new TreeBuilder();

        assertEquals(result,  builder.bestSplit(dataset));
    }

    @Test
    void PrintTreeTest () {
        List<List<Float>> dataset = new ArrayList<>();
        dataset.add(Arrays.asList(2.771244718f, 1.784783929f, 0f));
        dataset.add(Arrays.asList(1.728571309f, 1.169761413f, 0f));
        dataset.add(Arrays.asList(3.678319846f, 2.81281357f, 0f));
        dataset.add(Arrays.asList(3.961043357f, 2.61995032f, 0f));
        dataset.add(Arrays.asList(2.999208922f, 2.209014212f, 0f));
        dataset.add(Arrays.asList(7.497545867f, 3.162953546f, 1f));
        dataset.add(Arrays.asList(9.00220326f, 3.339047188f, 1f));
        dataset.add(Arrays.asList(7.444542326f, 0.476683375f, 1f));
        dataset.add(Arrays.asList(10.12493903f, 3.234550982f, 1f));
        dataset.add(Arrays.asList(6.642287351f, 3.319983761f, 1f));

        TreeBuilder builder = new TreeBuilder();

        Node root = builder.buildTree(dataset, 4, 1);

        builder.printTree(root, "");
    }


}